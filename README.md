# DL_POLY Installation Script

This bash script is created to install DL_Poly Classic software, Vesta and gnuplot for simulation course organizing by [Positron Foundation](https://www.positron.foundation). It works on Ubuntu/Debian based systems only. This installation script isn't associated with DL_Poly Classic, Vesta or gnuplot. 

#### [Click here to download this script](https://gitlab.com/athulvis/dlp-installation/-/archive/master/dlp-installation-master.zip)
                                                                      
### Official Links of Softwares                             
                                                                         					
DL_Poly Classic([Link to Gitlab repository](https://gitlab.com/DL_POLY_Classic/dl_poly/))         
                                                                             
Vesta ([link to Website](https://jp-minerals.org/vesta/))                              
                                                                       
Gnuplot ([Link to Website](http://www.gnuplot.info/))                                 
                                                                       
                                                                       
### Additional Softwares Installed            

1. Gfortran version 9                                      
2. Java - openjdk and openjre  version 14                  
3. mpich for multiprocessing                               
						    						                    
### How to Run     
                                                                       		  
                                                                       
1. Extract this zip file and open the folder. Open Terminal there (right click on the white space, choose `Open with Terminal`)

2. Run `sudo apt update` to update the repository softwares list. 
                                                                       
3. Run `chmod +x dl_poly_install.sh` for providing the permission to execute the script.

4. Run `./dl_poly_install.sh` to execute the installation script.
																        
4. That's all and if no error message is shown, the softwares are installed. You can run tests if the test script is provided.		
	                            										
Check the `logfile.txt` for installation log. In case of any errors, share the log file in the communication platform.
