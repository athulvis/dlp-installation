#!/bin/sh

#########################################################################
#  This code is written by Athul R.T [athul@disroot.org]                # 
#                                                                       #
#  It can be distributed according to GPLv3 license					    #
# [https://www.gnu.org/licenses/gpl-3.0.en.html]                 	    #
#                                                                       #
#       =============== Instructions===================                 #
#                                                                       #
#   This bash script is created to install DL_Poly Classic software,    #
#   Vesta and gnuplot for simulation course organizing by Positron      #
#  Foundation [www.positron.foundation]. It works on Ubuntu/Debian      #
#   based systems only. This installation script isn't                  #
#  associated with DL_Poly Classic, Vesta or gnuplot.                   #
#                                                                       #
#      ---Official Links of Softwares---                                #
#                                                                       #  					
#   DL_Poly Classic[https://gitlab.com/DL_POLY_Classic/dl_poly/]        #
#                                                                       #      
#   Vesta [https://jp-minerals.org/vesta/]                              #
#                                                                       #
#    Gnuplot [http://www.gnuplot.info/]                                 #
#                                                                       #
#                                                                       #
#           ---------Additional Softwares Installed---------            #
#                                                                       #
#            1. Gfortran version 9                                      #
#            2. Java - openjdk and openjre  version 14                  #
#            3. mpich for multiprocessing                               #
#						    						                    #
#		    --------------How to Run-----------------------             #
#                                                                       #		
#   1. Run `sudo apt update` to update the repository softwares list.   #
#                                                                       #
#	2. Copy this file to a new folder and open Terminal there (right 	#
#	click on the white space, choose Open with Terminal                 #
#                                                                       #
#   3. Run `chmod +x dl_poly_install.sh` for providing the permission   #
#   to execute the script												#
#																        #
#	4. That's all if no error message is shown, the softwares are   	#
#   installed. You can run tests if the test script is provifded.		#
#	                            										#
#	Check the logfile.txt for installation log. In case of any errors,  #
#   share the log file in the communication platform.					#
#                                                                       #
#########################################################################

# Creating the log file
log=logfile.txt

# append date to log file
date >> $log


if ! dpkg -s gnuplot >/dev/null 2>&1; then
  echo "gnuplot is not installed. Installing gnuplot..."
  sudo apt-get -y install gnuplot | tee -a $log
else 
    echo "Gnuplot Exists" | tee -a $log
fi

 
if ! dpkg -s gfortran-9 >/dev/null 2>&1; then
    echo "Gfortran doesn't exist. Installing Gfortran and mpich" | tee -a $log
    sudo apt-get -y install mpich gfortran-9 | tee -a $log
    sudo update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/gfortran-9 9
else
    sudo update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/gfortran-9 9
    sudo apt-get -y install mpich | tee -a $log
    echo "Gfortran Exists" 
fi

if ! dpkg -s openjdk-14-jdk:amd64 >/dev/null 2>&1; then
        echo "Java doesn't exist. Installing java" | tee -a $log
    sudo apt-get -y install openjdk-14-jdk openjdk-14-jre | tee -a $log
else
    echo "Java Exists" | tee -a $log
fi

vesta=VESTA-gtk3.tar.bz2
if [ ! -e "$vesta" ] ; then
    echo "Downloading Vesta" | tee -a $log
    wget -c https://jp-minerals.org/vesta/archives/3.5.2/$vesta | tee -a $log
fi

if [ ! -d "VESTA-gtk3" ]; then
  echo "Extracting Vesta" | tee -a $log
  tar -xvjf $vesta | tee -a $log
fi

file=dl_poly-RELEASE-1-9a.tar.gz

if [ ! -e "$file" ] ; then
    echo "Downloading DL_POLY Classic" | tee -a $log
    wget -c https://gitlab.com/DL_POLY_Classic/dl_poly/-/archive/RELEASE-1-9a/$file | tee -a $log
fi

if [ ! -d "dl_poly-RELEASE-1-9a" ]; then
  tar -xvzf $file | tee -a $log
fi
DIR="$(dirname "$(readlink -f "$0")")"

chmod u+x $DIR/VESTA-gtk3/VESTA


MAIN_DIR=$DIR/dl_poly-RELEASE-1-9a
BUILD_DIR=$DIR/dl_poly-RELEASE-1-9a/build
SOURCE_DIR=$DIR/dl_poly-RELEASE-1-9a/source
EXEC_DIR=$DIR/dl_poly-RELEASE-1-9a/execute

cp -r $BUILD_DIR/MakeSEQ $SOURCE_DIR/makefile

chmod u+x $SOURCE_DIR/makefile
#cd $SOURCE_DIR
make clean -C $SOURCE_DIR
make gfortran -C $SOURCE_DIR 2>&1 | tee -a $log
chmod o+r,o+x $EXEC_DIR
chmod o+r     $MAIN_DIR/manual
chmod o+r     $MAIN_DIR/utility

echo "The log is written to file $log . Please check it for further references. \n" 
